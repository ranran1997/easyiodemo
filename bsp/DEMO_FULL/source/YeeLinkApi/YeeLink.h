#ifndef __YEELINK_H__
#define __YEELINK_H__

int SetYeelinkApiKey(char *apikey);
int InitYeelink(void);
int PostYeelinkData(char *data);

#endif
