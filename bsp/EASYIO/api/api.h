﻿/**
 * @file                 api.h
 * @brief                EasyIO-GPRS 接口文件
 * @details		使用EasyIO Framework 需要引入此头文件，此文件包含了所有功能和方法的API函数和系统的钩子函数
 * @author               stopfan
 * @date         2013-11-11
 * @version      A001
 * @par Copyright (c): 
 *               新动力科技
 * @par History:         
 *       version: stopfan, 2013-12-12, desc\n
 */


#ifndef __easy_io_api_h__
#define __easy_io_api_h__

#include "api_type.h"
#include "api_result.h"
#include "init.h"


#define EI_MALLOC rt_malloc 		/**< 定义 EI_MALLOC	*/
#define EI_FREE rt_free			/**< 定义 EI_FREE	*/
#define EI_SLEEP rt_thread_sleep	/**< 定义 EI_SLEEP	*/
#define EI_DEBUG rt_kprintf		/**< Dbug		*/



/** 格式化输出字符串到串口
 *
 * @param[in] fmt : 格式化字符串
 *
 * @return
 **/
int ApiSerialPrintf(USART_PORT_NUM port , char *fmt, ...);
/** 将内容写入串口
 *
 * @param[in] buffer : 写入缓冲
 * @param[in] size : 写入长度
 *
 * @return
 **/
EIResult_t ApiWriteSerial(USART_PORT_NUM port , unsigned char *buffer , int size);


/** 读取EEPROM中的内容
 *
 * @param[in] pos : 要读取的位置
 * @param[out] buffer : 存储读出内容的缓冲
 * @param[in] size : 读取长度
 *
 * @return
 **/
EIResult_t ApiReadEEPROM(int pos , unsigned char *buffer , int size);

/** 将内容写入EEPROM
 *
 * @param[in] pos : 要写入的位置
 * @param[out] buffer : 写入缓冲
 * @param[in] size : 写入长度
 *
 * @return
 **/
EIResult_t ApiWriteEEPROM(int pos , unsigned char *buffer , int size);

/** 获取基站位置信息
 *
 * @return
 **/
EIResult_t ApiGetGsmLoc(struct GsmLocInfo *info);

/** 发送短信
 *
 * @param[in] phonenum : 目标手机号码
 * @param[in] body : 发送内容
 *
 * @return
 **/
EIResult_t ApiSendSms(const char *phonenum ,const char *body);

/** 发DTU送数据
 *
 * @param[in] buffer : 发送缓冲
 * @param[in] size : 发送长度
 *
 * @return
 **/
EIResult_t ApiSendTcpData(unsigned char *buffer , int size);

/** 发送P2p消息
 *
 * @param[in] imei : 目的IMEI
 * @param[in] body ：内容
 *
 * @return
 **/
EIResult_t ApiSendP2paMsg(const char *imei , const char *body);

/** 发送XMPP消息
 *
 * @param[in] to : 目的ID
 * @param[in] body ：内容
 *
 * @return
 **/
EIResult_t ApiSendXmppMsg(const char *to , const char *body);

/** HTTP请求
 *
 * @param[in] host : 主机
 * @param[in] port ：端口
 * @parma[in] request : 请求数据
 * @parma[in] reqlen : 数据请求长度
 * @parma[out] response : 请求内容
 * @parma[in] resplen : 请求缓冲长度
 * @parma[in] timeout : 超时时间 1秒为单位
 * @return int 返回长度
 **/
int ApiNetworkRequest(const char *host , const char *port , const char *request , const int reqlen , char *response , int resplen , int timeout);

/** 获取信号质量
 *
 * @return 返回信号数值 [0-30]
 **/
int ApiSignalValue(void);

/** 获取系统信息
 *
 * #param[out] info : 输出系统信息结构体
 * @return
 **/
EIResult_t ApiGetSysInfo(struct SysInfo *info);

/** 创建一个线程
 *
 * #param[in] StackSize 线程运行的栈空间
 * #param[in] ThreadEntry 线程函数入口
 * #param[in] ThreadParam 线程入口函数参数
 * @return 线程ID
 **/
unsigned int ApiCreateThread(int StackSize , void *ThreadEntry , void *ThreadParam);

/** 创建一个线程
 *
 * #param[in] TaskEntry 任务函数入口
 * #param[in] TaskParam 任务函数参数
 * @return 任务是否添加成功
 **/
EIResult_t ApiAddTask(void *TaskEntry , void *TaskParam);

/** 休眠百分之一妙,100个tick为1秒 ,api.Sleep(1); 10毫秒延迟 
 *
 * @return
 **/
void ApiSleep(int tick);

/** 微妙级延迟函数,高精度。
 *
 * @return
 **/
void ApiDelayUS(int us);

/** 打开看门狗
 *
 * @return
 **/
void ApiEnableWatchDog(void);
/** 设备重启 ,执行此函数后，模块和MCU都将重启
 *
 * @return
 **/
void ApiResetSystem(void);

/** GPRS 模块重启 ,只针对 gprs 模块进行复位重启
 *
 * @return
 **/
void ApiResetGprsMoudle(void);

/** 设置调试信息输出掩码
 *
 * @return
 **/
void ApiSetSysDebugMask(int mask);

void StartupImpl(void);
void RecvSysEventImpl(EI_SYS_EVENT event , void *body);


#endif
