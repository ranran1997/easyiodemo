#include "api/api.h"
#include "api/init.h"

#include <string.h>
#include <stdio.h>

#include <rtthread.h>

#define DTU_SERIAL_PORT USART_PORT_1 //使用串口1作为数据传输口


static void recv_serial_data(USART_PORT_NUM port,unsigned char *buffer , unsigned int size)
{
	
}

void  StartupImpl(void)
{
	unsigned char eeprom_buf[32] = {0x0};
	unsigned char eeprom_str[] = "TEST EEPROM !!!";
	ApiWriteEEPROM(0,eeprom_str,sizeof(eeprom_str));
	ApiReadEEPROM(0,eeprom_buf,sizeof(eeprom_str));
	ApiSerialPrintf(USART_PORT_1,"READ EEPROM STR : [ %s ]\n",eeprom_buf);

}
void RecvSysEventImpl(EI_SYS_EVENT event , void *body)
{

	switch(event)
	{
		case RECVEVENT_SERIAL: //串口有信息到来
			recv_serial_data(
					((struct RcvSerialDataEvent*)body)->port,
					((struct RcvSerialDataEvent*)body)->data,
					((struct RcvSerialDataEvent*)body)->len
					);
			break;
		default:
			break;
	}
	//
}

